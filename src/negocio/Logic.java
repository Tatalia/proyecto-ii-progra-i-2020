/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;
import datos.Validations;

/**
 *
 * @author nati2
 */
public class Logic {
    
    Validations validations = new Validations();
    
    public Boolean ifTheUserExists(String userIdentifier , String passwordInsertedByUser){
        return validations.userExist(userIdentifier, passwordInsertedByUser);
    }
    
    public Boolean isAdmin(String userIdentifier){
        return validations.isAdmin(userIdentifier);
    }
    
}
