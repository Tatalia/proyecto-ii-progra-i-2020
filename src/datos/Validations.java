/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author nati2
 */
public class Validations {
    
    public Boolean userExist(String userIdentifier , String passwordInsertedByUser) {
        boolean result = false;
        File entityFile = new File("users.txt");
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }
            
            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);
                if (tempArray[3].equals(passwordInsertedByUser) && tempArray[0].equals(userIdentifier)) {
                    result = true;
                }
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: users.txt\n" + ex);
        }
        return result;
    }
    
    public Boolean isAdmin(String userIdentifier) {
        boolean result = false;
        File entityFile = new File("users.txt");
        try {
            BufferedReader entity = new BufferedReader(new FileReader(entityFile));
            ArrayList<String> entityArray = new ArrayList<>();
            while (entity.ready()) {
                entityArray.add(entity.readLine());
            }       
            for (String singleEntity : entityArray) {
                String[] tempArray = singleEntity.split(",", 0);
                if (tempArray[0].equals(userIdentifier)) {
                    if (tempArray[4].equals("0")){
                        result = true;
                    }          
                }
            }
            entity.close();
        } catch (Exception ex) {
            System.out.println("Error al leer archivo: users.txt\n" + ex);
        }
        return result;
    }    
}
